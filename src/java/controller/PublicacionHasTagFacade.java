/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import model.PublicacionHasTag;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author SoftVargas
 */
@Stateless
public class PublicacionHasTagFacade extends AbstractFacade<PublicacionHasTag> {
    @PersistenceContext(unitName = "ProyectoForoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PublicacionHasTagFacade() {
        super(PublicacionHasTag.class);
    }
    
}
