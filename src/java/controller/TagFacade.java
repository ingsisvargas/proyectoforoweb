/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import model.Tag;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author SoftVargas
 */
@Stateless
public class TagFacade extends AbstractFacade<Tag> {

    @PersistenceContext(unitName = "ProyectoForoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TagFacade() {
        super(Tag.class);
    }

    public List<Tag> cargarTodo() {
        return (List<Tag>) this.getEntityManager().createNamedQuery("Tag.findAll").getResultList();
    }

    public List<Tag> cargarTagsFiltro(String query) {
        return (List<Tag>) this.getEntityManager().createNamedQuery("Tag.findFiltro").setParameter("filtro","'%"+query+"'").getResultList();
    }

}
