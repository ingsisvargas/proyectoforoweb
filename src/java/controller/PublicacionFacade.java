/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import model.Publicacion;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.Comentario;
import model.PublicacionHasTag;
import model.Tag;
import model.Usuario;

/**
 *
 * @author SoftVargas
 */
@Stateless
public class PublicacionFacade extends AbstractFacade<Publicacion> {
    @EJB
    private ComentarioFacade comentarioFacade;

    @EJB
    private TagFacade tagFacade;
    @PersistenceContext(unitName = "ProyectoForoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PublicacionFacade() {
        super(Publicacion.class);
    }

    /**
     * Permite cargar un listado con las publicaciones de tipo pregunta mas
     * puntuadas
     *
     * @return
     */
    public List<Publicacion> cargarPreguntasPuntuadas() {
        List<Publicacion> publicaciones = this.getEntityManager().createNamedQuery("Publicacion.findPublicacionesPuntuadas").setParameter("tipo", "Pregunta").setMaxResults(20).getResultList();
        return publicaciones;

    }

    /**
     * Permite cargar un listado con las publiaciones de tipo pregunta mas
     * recientes
     *
     * @return
     */
    public List<Publicacion> cargarPreguntasRecientes() {
        List<Publicacion> publicaciones = this.getEntityManager().createNamedQuery("Publicacion.findPublicacionesRecientes").setParameter("tipo", "Pregunta").setMaxResults(20).getResultList();
        return publicaciones;

    }

    /**
     * permite cargar un listado con las publicaciones de tipo experiencia mas
     * recientes
     *
     * @return
     */
    public List<Publicacion> cargarExperienciasRecientes() {
        List<Publicacion> publicaciones = this.getEntityManager().createNamedQuery("Publicacion.findPublicacionesRecientes").setParameter("tipo", "Experiencia").setMaxResults(20).getResultList();
        return publicaciones;
    }

    /**
     * Permite cargar un listado con las publicaciones de tipo experiencia mas
     * puntuadas
     *
     * @return
     */
    public List<Publicacion> cargarExperienciasPuntuadas() {
        List<Publicacion> publicaciones = this.getEntityManager().createNamedQuery("Publicacion.findPublicacionesPuntuadas").setParameter("tipo", "Experiencia").setMaxResults(20).getResultList();
        return publicaciones;

    }

    /**
     * Permite cargar un listado de publicaciones de cualquier tipo, a partir de
     * un tag
     *
     * @param tag
     * @return
     */
    public List<Publicacion> buscarPublicaciones(String tag) {
        List<Publicacion> publicaciones = this.getEntityManager().createNamedQuery("Publicacion.findAll").getResultList();
        List<Publicacion> resultado = new ArrayList<>();

        for (Publicacion p : publicaciones) {
            for (Tag ph : p.getPublicacionHasTagList()) {
                if (ph.getDescipcion().equals(tag)) {
                    resultado.add(p);
                    break;
                }
            }
        }

        return resultado;
    }

    public void crearNuevoTag(Tag nuevo) {
        this.tagFacade.create(nuevo);
    }

    public List<Tag> cargarTags() {
        return this.tagFacade.cargarTodo();
    }

    public List<Tag> cargarTags(String query) {
        return this.tagFacade.cargarTagsFiltro(query);
    }

    public List<Publicacion> listarPublicacionesUsuario(Usuario user) {
        return this.getEntityManager().createNamedQuery("Publicacion.findByUser").setParameter("usuario", user).getResultList();
    }

    public List<Publicacion> cargarPreguntasPuntuadasUsuario(Usuario usuario) {
        List<Publicacion> publicaciones = this.getEntityManager().createNamedQuery("Publicacion.findPublicacionesPuntuadasUsuario").setParameter("tipo", "Pregunta").setParameter("usuario", usuario).setMaxResults(20).getResultList();
        return publicaciones;

    }

    public List<Publicacion> cargarPreguntasRecientesUsuario(Usuario usuario) {
        List<Publicacion> publicaciones = this.getEntityManager().createNamedQuery("Publicacion.findPublicacionesRecientesUsuario").setParameter("tipo", "Pregunta").setParameter("usuario", usuario).setMaxResults(20).getResultList();
        return publicaciones;
    }

    public List<Publicacion> cargarExperienciasRecientesUsuario(Usuario usuario) {
        List<Publicacion> publicaciones = this.getEntityManager().createNamedQuery("Publicacion.findPublicacionesRecientesUsuario").setParameter("tipo", "Experiencia").setParameter("usuario", usuario).setMaxResults(20).getResultList();
        return publicaciones;
    }

    public List<Publicacion> cargarExperienciasPuntuadasUsuario(Usuario usuario) {
        List<Publicacion> publicaciones = this.getEntityManager().createNamedQuery("Publicacion.findPublicacionesPuntuadasUsuario").setParameter("tipo", "Experiencia").setParameter("usuario", usuario).setMaxResults(20).getResultList();
        return publicaciones;

    }

    public void agregarComentario(Comentario comentarioPublicacion) {
    this.comentarioFacade.create(comentarioPublicacion);
    
    }
}
