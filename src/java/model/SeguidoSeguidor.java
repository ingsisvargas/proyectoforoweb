/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SoftVargas
 */
@Entity
@Table(name = "seguido_seguidor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SeguidoSeguidor.findAll", query = "SELECT s FROM SeguidoSeguidor s"),
    @NamedQuery(name = "SeguidoSeguidor.findById", query = "SELECT s FROM SeguidoSeguidor s WHERE s.id = :id")})
public class SeguidoSeguidor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "seguido", referencedColumnName = "codigo")
    @ManyToOne(optional = false)
    private Usuario seguido;
    @JoinColumn(name = "seguidor", referencedColumnName = "codigo")
    @ManyToOne(optional = false)
    private Usuario seguidor;

    public SeguidoSeguidor() {
    }

    public SeguidoSeguidor(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Usuario getSeguido() {
        return seguido;
    }

    public void setSeguido(Usuario seguido) {
        this.seguido = seguido;
    }

    public Usuario getSeguidor() {
        return seguidor;
    }

    public void setSeguidor(Usuario seguidor) {
        this.seguidor = seguidor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SeguidoSeguidor)) {
            return false;
        }
        SeguidoSeguidor other = (SeguidoSeguidor) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.SeguidoSeguidor[ id=" + id + " ]";
    }
    
}
