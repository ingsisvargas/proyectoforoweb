/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SoftVargas
 */
@Entity
@Table(name = "publicacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Publicacion.findPublicacionesPuntuadasUsuario", query = "SELECT p FROM Publicacion p where p.tipo=:tipo AND p.usuario=:usuario  ORDER BY p.calificacion DESC"),
    @NamedQuery(name = "Publicacion.findPublicacionesRecientesUsuario", query = "SELECT p FROM Publicacion p where p.tipo=:tipo AND p.usuario=:usuario  ORDER BY p.fecha DESC"),
    @NamedQuery(name = "Publicacion.findByUser", query = "SELECT p FROM Publicacion p WHERE p.usuario=:usuario ORDER BY p.fecha DESC"),
    @NamedQuery(name = "Publicacion.findAll", query = "SELECT p FROM Publicacion p"),
    @NamedQuery(name = "Publicacion.findPublicacionesPuntuadas", query = "SELECT p FROM Publicacion p where p.tipo=:tipo ORDER BY p.calificacion DESC"),
    @NamedQuery(name = "Publicacion.findPublicacionesRecientes", query = "SELECT p FROM Publicacion p where p.tipo=:tipo ORDER BY p.fecha DESC"),
    @NamedQuery(name = "Publicacion.findById", query = "SELECT p FROM Publicacion p WHERE p.id = :id"),
    @NamedQuery(name = "Publicacion.findByTitulo", query = "SELECT p FROM Publicacion p WHERE p.titulo = :titulo"),
    @NamedQuery(name = "Publicacion.findByFecha", query = "SELECT p FROM Publicacion p WHERE p.fecha = :fecha"),
    @NamedQuery(name = "Publicacion.findByTipo", query = "SELECT p FROM Publicacion p WHERE p.tipo = :tipo"),
    @NamedQuery(name = "Publicacion.findByCalificacion", query = "SELECT p FROM Publicacion p WHERE p.calificacion = :calificacion")})
public class Publicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Size(max = 125)
    @Column(name = "titulo")
    private String titulo;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Lob
    @Size(max = 65535)
    @Column(name = "contenido")
    private String contenido;
    @Size(max = 15)
    @Column(name = "tipo")
    private String tipo;
    @Column(name = "calificacion")
    private Integer calificacion;
    @ManyToMany
    @JoinTable(
            name = "publicacion_has_tag",
            joinColumns = {
                @JoinColumn(name = "publicacion_id", referencedColumnName = "ID")},
            inverseJoinColumns = {
                @JoinColumn(name = "tag_id", referencedColumnName = "ID")})
    private List<Tag> tags;
    @JoinColumn(name = "usuario", referencedColumnName = "codigo")
    @ManyToOne(optional = false)
    private Usuario usuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "publicacion")
    private List<Comentario> comentarioList;

    public Publicacion() {
    }

    public Publicacion(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getFecha() {
        String fech = "";
        String dia = this.stringDia(fecha.getDay());
        String mes = this.stringMes(fecha.getMonth());

        fech = dia + " " + fecha.getDate() + " de " + mes + " de " + (fecha.getYear() + 1900);
        return fech;
    }

    public String stringMes(int mes) {
        String mess = "";
        switch (mes) {
            case 1:
                mess = "Enero";
                break;
            case 2:
                mess = "Febrero";
                break;
            case 3:
                mess = "Marzo";
                break;
            case 4:
                mess = "Abril";
                break;
            case 5:
                mess = "Mayo";
                break;
            case 6:
                mess = "Junio";
                break;
            case 7:
                mess = "Julio";
                break;
            case 8:
                mess = "Agosto";
                break;
            case 9:
                mess = "Septiembre";
                break;
            case 10:
                mess = "Octubre";
                break;
            case 11:
                mess = "Noviembre";
                break;
            case 12:
                mess = "Diciembre";
                break;
        }
        return mess;
    }

    public String stringDia(int dia) {
        String diaa = "";
        switch (dia) {
            case 0:
                diaa = "Domingo";
                break;
            case 1:
                diaa = "Lunes";
                break;
            case 2:
                diaa = "Martes";
                break;
            case 3:
                diaa = "Miercoles";
                break;
            case 4:
                diaa = "Jueves";
                break;
            case 5:
                diaa = "Viernes";
                break;
            case 6:
                diaa = "Sabado";
                break;
        }
        return diaa;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    @XmlTransient
    public List<Tag> getPublicacionHasTagList() {
        return tags;
    }

    public void setPublicacionHasTagList(List<Tag> tags) {
        this.tags = tags;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @XmlTransient
    public List<Comentario> getComentarioList() {
        return comentarioList;
    }

    public void setComentarioList(List<Comentario> comentarioList) {
        this.comentarioList = comentarioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Publicacion)) {
            return false;
        }
        Publicacion other = (Publicacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Publicacion[ id=" + id + " ]";
    }
}
