/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SoftVargas
 */
@Entity
@Table(name = "publicacion_has_tag")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PublicacionHasTag.findAll", query = "SELECT p FROM PublicacionHasTag p"),
    @NamedQuery(name = "PublicacionHasTag.findById", query = "SELECT p FROM PublicacionHasTag p WHERE p.id = :id")})
public class PublicacionHasTag implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id @GeneratedValue 
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "tag_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Tag tagId;
    @JoinColumn(name = "publicacion_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Publicacion publicacionId;

    public PublicacionHasTag() {
    }

    public PublicacionHasTag(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Tag getTagId() {
        return tagId;
    }

    public void setTagId(Tag tagId) {
        this.tagId = tagId;
    }

    public Publicacion getPublicacionId() {
        return publicacionId;
    }

    public void setPublicacionId(Publicacion publicacionId) {
        this.publicacionId = publicacionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PublicacionHasTag)) {
            return false;
        }
        PublicacionHasTag other = (PublicacionHasTag) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.PublicacionHasTag[ id=" + id + " ]";
    }
    
}
