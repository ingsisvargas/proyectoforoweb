package controllerView;

import model.Publicacion;
import controllerView.util.JsfUtil;
import controllerView.util.PaginationHelper;
import controller.PublicacionFacade;
import java.awt.event.ActionEvent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import model.Comentario;
import model.PublicacionHasTag;
import model.Tag;
import model.Usuario;
import org.primefaces.event.CloseEvent;

@Named("publicacionController")
@SessionScoped
public class PublicacionController implements Serializable {

    private Publicacion current;
    private DataModel items = null;
    @EJB
    private controller.PublicacionFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private List<Publicacion> preguntasRecientes;
    private List<Publicacion> preguntasPuntuadas;
    private List<Publicacion> experienciasRecientes;
    private List<Publicacion> experienciasPuntuadas;
    private List<Publicacion> publicacionesEncontradas;
    private List<Publicacion> publicacionesUsuario;
    private List<Publicacion> publicacionesPreguntasRecientesVisto;
    private List<Publicacion> publicacionesPreguntasPuntuadasVisto;
    private List<Publicacion> publicacionesExperienciasRecientesVisto;
    private List<Publicacion> publicacionesExperienciasPuntuadasVisto;
    private String tag = "";
    private List<Tag> tagPublicacion = new ArrayList<Tag>();
    private Tag tagNuevo;
    private Publicacion publicacionVer;
    private Comentario comentarioPublicacion;

    public PublicacionController() {
    }

   
    
    
    public List<Publicacion> getPublicacionesPreguntasRecientesVisto() {
        FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        Usuario usuario = (Usuario) session.getAttribute("usuarioVisto");
        this.publicacionesPreguntasRecientesVisto = ejbFacade.cargarPreguntasRecientesUsuario(usuario);
        return publicacionesPreguntasRecientesVisto;
    }

    public List<Publicacion> getPublicacionesPreguntasPuntuadasVisto() {
        FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        Usuario usuario = (Usuario) session.getAttribute("usuarioVisto");
        this.publicacionesPreguntasPuntuadasVisto = ejbFacade.cargarPreguntasPuntuadasUsuario(usuario);
        return publicacionesPreguntasPuntuadasVisto;
    }

    public List<Publicacion> getPublicacionesExperienciasRecientesVisto() {
        FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        Usuario usuario = (Usuario) session.getAttribute("usuarioVisto");
        this.publicacionesExperienciasRecientesVisto = ejbFacade.cargarExperienciasRecientesUsuario(usuario);
        return publicacionesExperienciasRecientesVisto;
    }

    public List<Publicacion> getPublicacionesExperienciasPuntuadasVisto() {
        FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        Usuario usuario = (Usuario) session.getAttribute("usuarioVisto");
        this.publicacionesExperienciasPuntuadasVisto = ejbFacade.cargarExperienciasPuntuadasUsuario(usuario);
        return publicacionesExperienciasPuntuadasVisto;
    }
    
    public void agregarComentario(){
        this.comentarioPublicacion.setPublicacion(publicacionVer);
        FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        Usuario user = (Usuario) session.getAttribute("usuario");
        this.comentarioPublicacion.setUsuario(user);
        Date fecha= new Date();
        comentarioPublicacion.setFecha(fecha);
        this.ejbFacade.agregarComentario(comentarioPublicacion);
    
    }

    public Comentario getComentarioPublicacion() {
        return comentarioPublicacion;
    }

    public void setComentarioPublicacion(Comentario comentarioPublicacion) {
        this.comentarioPublicacion = comentarioPublicacion;
    }
    
    public List<Tag> getTagPublicacion() {
        return tagPublicacion;
    }

    public Tag getTagNuevo() {
        return tagNuevo;
    }

    public void setTagNuevo(Tag tagNuevo) {
        this.tagNuevo = tagNuevo;
    }

    public void setTagPublicacion(List<Tag> tagPublicacion) {
        this.tagPublicacion = tagPublicacion;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Publicacion getPublicacionVer() {
        return publicacionVer;
    }

    public void setPublicacionVer(Publicacion publicacionVer) {
        this.publicacionVer = publicacionVer;
    }

    @PostConstruct
    public void init() {
        this.tagNuevo = new Tag();
        this.current = new Publicacion();
        this.publicacionVer = new Publicacion();
        this.comentarioPublicacion=new Comentario();
    }

    public List<Publicacion> getPublicacionesUsuario() {
        FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        Usuario user = (Usuario) session.getAttribute("usuario");
        publicacionesUsuario = ejbFacade.listarPublicacionesUsuario(user);
        return publicacionesUsuario;
    }

    public void cargarPublicacionVer(Publicacion pub) {
        this.publicacionVer = pub;
        this.publicacionVer.setCalificacion(publicacionVer.getCalificacion()+1);
        this.ejbFacade.edit(publicacionVer);
    }

    public void setPublicacionesUsuario(List<Publicacion> publicacionesUsuario) {
        this.publicacionesUsuario = publicacionesUsuario;
    }

    public void limpiarBusqueda() {
        this.tag = "";
        this.publicacionesEncontradas = null;
    }

    public List<Publicacion> getPublicacionesEncontradas() {
        return publicacionesEncontradas;
    }

    public List<Tag> completeTags(String query) {
        List<Tag> allTags = ejbFacade.cargarTags();
        List<Tag> filteredTags = new ArrayList<>();
        
        for (int i = 0; i < allTags.size(); i++) {
            Tag tagSel = allTags.get(i);
            if (tagSel.getDescipcion().toLowerCase().startsWith(query)) {
                filteredTags.add(tagSel);
            }
        }
        return filteredTags;
    }

    /**
     * Perrmite Cargar un listado con todas las publicaciones de cualquier tipo
     * que esten asociadas al tag de busqueda, para su posterior visualizacion
     * en GUI
     */
    public void buscarPublicaciones() {
        this.publicacionesEncontradas = this.ejbFacade.buscarPublicaciones(tag);
    }

    public List<Publicacion> getPreguntasRecientes() {
        this.cargarPreguntasRecientes();
        return preguntasRecientes;
    }

    public List<Publicacion> getPreguntasPuntuadas() {
        this.cargarPreguntasPuntuadas();
        return preguntasPuntuadas;
    }

    public List<Publicacion> getExperienciasRecientes() {
        this.cargarExperienciasRecientes();
        return experienciasRecientes;
    }

    public List<Publicacion> getExperienciasPuntuadas() {
        this.cargarExperienciasPuntuadas();
        return experienciasPuntuadas;
    }

    /**
     * Metodo que carga las 20 publicaciones con mayor calificacion,para su
     * posterior visualizacion en GUI
     */
    private void cargarPreguntasPuntuadas() {
        preguntasPuntuadas = ejbFacade.cargarPreguntasPuntuadas();

    }

    public boolean nuevoTag() {
        this.ejbFacade.crearNuevoTag(tagNuevo);
        tagNuevo.setDescipcion("");
        return true;

    }

    public Publicacion getSelected() {
        if (current == null) {
            current = new Publicacion();
            selectedItemIndex = -1;
        }
        return current;
    }

    private PublicacionFacade getFacade() {
        return ejbFacade;
    }

    public boolean cargar() {
        return true;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Publicacion) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Publicacion();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            return prepareCreate();
        } catch (javax.ejb.EJBException e) {
            System.out.println("CAUSA: " + e.getCausedByException().getMessage());
            return null;
        }
    }

    public String prepareEdit() {
        current = (Publicacion) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PublicacionUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            current = new Publicacion();
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PublicacionDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Publicacion getPublicacion(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    /**
     * Permite cargar un conjunto de las 20 publicaciones de tipo pregunta mas
     * recientes, para su posterior visualizacion en GUI
     */
    private void cargarPreguntasRecientes() {
        this.preguntasRecientes = this.ejbFacade.cargarPreguntasRecientes();

    }

    public void destroy(Publicacion pub) {
        this.current = pub;
        this.performDestroy();
        FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        Usuario user = (Usuario) session.getAttribute("usuario");
        this.publicacionesUsuario = ejbFacade.listarPublicacionesUsuario(user);
        this.addMessage("PUBLICACION ELIMINADA!!");

    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void crear() {
        this.current.setPublicacionHasTagList(tagPublicacion);
        Date fecha = new Date();
        FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        Usuario user = (Usuario) session.getAttribute("usuario");
        this.current.setUsuario(user);
        this.current.setFecha(fecha);
        this.current.setCalificacion(0);
        this.create();
        this.tagPublicacion.removeAll(tagPublicacion);
        addMessage("PUBLICACION REGISTRADA!!");
    }

    /**
     * Permite cargar un conjunto de las 20 publicaciones de tipo experiencia
     * mas reciente, para su posterior visualizacion en GUI
     */
    private void cargarExperienciasRecientes() {
        this.experienciasRecientes = this.ejbFacade.cargarExperienciasRecientes();

    }

    /**
     * Permite cargar un conjunto de las 20 publicaciones de tipo experiencia
     * mas puntuadas para su posterior visualizacion en GUI
     */
    private void cargarExperienciasPuntuadas() {
        this.experienciasPuntuadas = this.ejbFacade.cargarExperienciasPuntuadas();
    }

    @FacesConverter(forClass = Publicacion.class)
    public static class PublicacionControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PublicacionController controller = (PublicacionController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "publicacionController");
            return controller.getPublicacion(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Publicacion) {
                Publicacion o = (Publicacion) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Publicacion.class.getName());
            }
        }
    }

    /**
     * Metodo que carga Has Tags de prueba para una publicacion
     *
     * @return datos, list que contiene los hasTags
     */
    private List<PublicacionHasTag> cargarHastagPrueba() {
        List<PublicacionHasTag> datos = new ArrayList<>();
        PublicacionHasTag ph1 = new PublicacionHasTag();
        Tag t = new Tag();
        t.setDescipcion("prueba");
        ph1.setTagId(t);
        datos.add(ph1);
        return datos;
    }
}
