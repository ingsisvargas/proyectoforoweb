/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Notifica el exito de registrar un usuario en el sistema.
 * @returns {undefined}
 */
function notificarRegistroUsuario(){
 alert("REGISTRO DE USAURIO EXITOSO!!!");    
}

function verDetalle(){
    PF('dlg1').show();
}
/**
 * Permite gestionar el informe de exito al terminar la transaccion de 
 * eliminar una publicacion
 * @param {type} data
 * @returns {undefined}
 */
function notificarEliminarPublicacion(data) {

    var status = data.status; // Can be "begin", "complete" or "success".

    switch (status) {
        case "begin": // Before the ajax request is sent.
            // ...
            break;

        case "complete": // After the ajax response is arrived.

            break;

        case "success": // After update of HTML DOM based on ajax response..

            alert("PUBLICACION ELIMINADA CON EXITO!!");
            break;
    }
}

/**
 * Permite notificar el Registro de una publicacion
 * @param {type} data
 * @returns {undefined}
 */
function notificarRegistrarPublicacion(data) {

    var status = data.status; // Can be "begin", "complete" or "success".

    switch (status) {
        case "begin": // Before the ajax request is sent.
            // ...
            break;

        case "complete": // After the ajax response is arrived.

            break;

        case "success": // After update of HTML DOM based on ajax response..

            alert("PUBLICACION REGISTRADA CON EXITO!!");
            break;
    }
}

